const databaseURL = 'mongodb://localhost/database';
const databasePort = 3000;

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


Reservation = require('./models/reservation');
validators = require('./models/modelsValidators');

mongoose.connect(databaseURL, { useNewUrlParser: true });
const db = mongoose.connection;

app.get('/', function (req, res) {
    res.send("Please use /api/reservations");
});

app.get('/api/reservations', function (req, res) {
    Reservation.getReservations(function (err, reservations) {
        if (err) {
            throw err;
        }
        res.json(reservations);
    });
});

app.get('/api/reservations/:_id', function (req, res) {
    Reservation.getReservationById(req.params._id, function (err, reservation) {
        if (err) {
            throw err;
        }
        res.json(reservation);
    });
});

app.post('/api/reservations', function (req, res) {

    const reservation = req.body;

    if (validators.isReservationValid(reservation)) {
        Reservation.addReservation(reservation, function (err, reservation) {
            if (err) {
                throw err;
            }
            res.json(reservation);
        });
    }
});

app.put('/api/reservations/:_id', function (req, res) {
    const reservation = req.body;
    const id = req.params._id;
    Reservation.updateReservation(id, reservation, {}, function (err, reservation) {
        if (err) {
            throw err;
        }
        res.json(reservation);
    });
});

app.delete('/api/reservations/:_id', function (req, res) {
    const id = req.params._id;
    Reservation.deleteReservation(id, function (err, reservation) {
        if (err) {
            throw err;
        }
        res.json(reservation);
    });
});

app.listen(databasePort);
console.log(`Running on port ${databasePort}`);


