module.exports.isReservationValid = function (data) {
    const nameValidationRegExp = /^[\D]{2,30}$/;
    const dateValidationRegExp = /^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/;
    const emailValidationRegExp = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;


    if (!nameValidationRegExp.test(data["firstName"])) {
        return false;
    }
    if (!nameValidationRegExp.test(data["lastName"])) {
        return false;
    }
    if (!emailValidationRegExp.test(data["email"])) {
        return false;
    }
    if (!dateValidationRegExp.test(data["date"])) {
        return false;
    }
    return true;
}