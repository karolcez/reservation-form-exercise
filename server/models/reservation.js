const mongoose = require('mongoose');

const reservationSchema = mongoose.Schema({
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
})

const Reservation = module.exports = mongoose.model("Reservation", reservationSchema);

module.exports.getReservations = function (callback, limit) {
    Reservation.find(callback).limit(limit);
}

module.exports.getReservationById = function (id, callback) {
    Reservation.findById(id, callback);
}

module.exports.addReservation = function (reservation, callback) {

    Reservation.create(reservation, callback);
}

module.exports.updateReservation = function (id, reservation, options, callback) {
    const query = { _id: id };
    const update = {
        first_name: reservation.first_name,
        last_name: reservation.last_name,
        email: reservation.email,
        date: reservation.date
    }
    Reservation.findOneAndUpdate(query, update, options, callback);
}

module.exports.deleteReservation = function (id, callback) {
    const query = { _id: id };
    Reservation.remove(query, callback);
}