import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import ReservationForm from "./form/ReservationForm";
import { Provider } from 'react-redux';
import configureStore from '../store/configureStore';
import { mount, shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import moment from 'moment';

configure({ adapter: new Adapter() });
const store = configureStore();

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>, div);
  ReactDOM.unmountComponentAtNode(div);
});
describe('registration form basic tests', () => {

  it('form validation correct', () => {
   
    const exampleState = {
      firstName: "John",
      lastName: "Doe",
      email: "email@example.com",
      date: moment()
    }

    const wrapper = mount(
      <Provider store={configureStore(exampleState)}>
        <App />
      </Provider>);

    const saveButton = wrapper.find('button');
    const nameInput = wrapper.find('input').first();



    expect(saveButton.prop('type')).toBe('submit');
    expect(saveButton.prop('disabled')).toBe(false);

    saveButton.simulate('click');
  });

  it('form validation not correct', () => {
    const wrapper = mount(
      <Provider store={store}>
        <App />
      </Provider>);

    const saveButton = wrapper.find('button');
    const nameInput = wrapper.find('input').first();

    expect(saveButton.prop('type')).toBe('submit');
    expect(saveButton.prop('disabled')).toBe(true);
  });
});
