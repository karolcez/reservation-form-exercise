import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainActions from '../../actions/actions';
import ReservationFormView from "./ReservationFormView";
import { isMoment } from 'moment';
import lodash from 'lodash';
import toastr from 'toastr';
import { nameValidationRegExp, emailValidationRegExp } from "./formValidationRegExps";

export class ReservationForm extends Component {

    constructor() {
        super();

        this.handleChange = this.handleChange.bind(this);
        this.submitForm = this.submitForm.bind(this);

    }

    isFormValid(data) {
        for (const element of data) {
            if (element.name == "firstName" || element.name == "lastName") {
                if (!nameValidationRegExp.test(element.value)) {
                    return false;
                }
            }
            else if (element.name == "email") {
                if (!emailValidationRegExp.test(element.value)) {
                    return false;
                }
            }
        }
        return true;
    }

  

    submitForm(event) {

        event.preventDefault();
        const formData = {};
      
        if (this.isFormValid(event.target)) {
            for (const element of event.target) {
               
                formData[lodash.snakeCase(element.name)] = element.value;

            };

            fetch("http://localhost:3000/api/reservations", {
                method: "POST",
                body: JSON.stringify(formData),
                headers: { 'Content-Type': "application/json", 'Accept': 'application/json' }

            })
                .then(res => res.json())
                .then(res => {
                    this.props.actions.clearReservationFormState();
                    toastr.success("Reservation complete.");
                })
                .catch(() => {
                    toastr.error("Something went wrong. Reservation not complete");
                })
        }
    }

    handleChange(event) {
        if (isMoment(event)) {
            this.props.actions.updateReservationFormState({ date: event });
        }
        else {
            this.props.actions.updateReservationFormState(
                { [event.target.name]: event.target.value });
        }
    }

    render() {
        return (
            <ReservationFormView
                formState={this.props}
                handleChange={this.handleChange}
                submitForm={this.submitForm}
            />
        )
    }
}

function mapStateToProps(state) {

    return {
        ...state
    };
}

function mapDispatchToProps(dispatch) {

    return {
        actions: bindActionCreators(mainActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ReservationForm);