import React from 'react';
import { Button, FormGroup, ControlLabel, FormControl, HelpBlock } from "react-bootstrap";
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { nameValidationRegExp, emailValidationRegExp } from "./formValidationRegExps";

function validateName(input) {
    var regex = nameValidationRegExp;
    if (input.length == 0) {
        return null
    }
    if (regex.test(input)) {
        return 'success'
    }
    else {
        return 'error';
    }
}

function validateEmail(input) {
    var regex = emailValidationRegExp;
    if (input.length == 0) {
        return null
    }
    if (regex.test(input)) {
        return 'success'
    }
    else {
        return 'error';
    }
}

const ReservationFormView = ({ formState, handleChange, submitForm }) => {
    return (
        <form id="reservationForm" onSubmit={submitForm}>
            <h1>Reservation form</h1>
            <FormGroup
                controlId="formBasicText"
                validationState={validateName(formState.firstName)}
            >
                <ControlLabel>First name</ControlLabel>
                <FormControl
                    type="text"
                    value={formState.firstName}
                    placeholder="Enter first name"
                    onChange={handleChange}
                    name="firstName"
                />
                <FormControl.Feedback />
                <HelpBlock>First name should be with 2-30 characters of alphabet.</HelpBlock>
            </FormGroup>

            <FormGroup
                controlId="formBasicText"
                validationState={validateName(formState.lastName)}
            >
                <ControlLabel>Last name</ControlLabel>
                <FormControl
                    type="text"
                    value={formState.lastName}
                    placeholder="Enter last name"
                    onChange={handleChange}
                    name="lastName"
                />
                <FormControl.Feedback />
                <HelpBlock>Last name should be with 2-30 characters of alphabet.</HelpBlock>
            </FormGroup>

            <FormGroup
                controlId="formBasicText"
                validationState={validateEmail(formState.email)}
            >
                <ControlLabel>Email</ControlLabel>
                <FormControl
                    type="text"
                    value={formState.email}
                    placeholder="Enter email"
                    onChange={handleChange}
                    name="email"
                />
                <FormControl.Feedback />
            </FormGroup>

            <FormGroup
                controlId="formBasicText"
            >
                <ControlLabel>Event date</ControlLabel>
                <DatePicker
                    selected={formState.date}
                    onChange={handleChange}
                    name="date"

                />
                <FormControl.Feedback
                    type="date"
                />
            </FormGroup>

            <Button
                disabled={
                    !((validateName(formState.firstName) == "success") &&
                        (validateName(formState.lastName) == "success") &&
                        (validateName(formState.email) == "success"))
                }
                type="submit">Submit</Button>
        </form>
    );
}

export default ReservationFormView;
