import React, { Component } from 'react';
import ReservationForm from "./form/ReservationForm";

class App extends Component {
  render() {
    return (
      <div>
        <ReservationForm />
      </div>
    );
  }
}

export default App;
