import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import Bootstrap from 'bootstrap/dist/css/bootstrap.css';
import styles from "./styles/style.css";
import toastr from "./styles/toastr.css";

const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
