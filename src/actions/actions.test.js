import expect from 'expect';
import * as types from './actionTypes';
import * as actions from './actions';

describe('actions basic tests', () => {
    const exampleValue = Symbol();
    const exampleName = Symbol();
    const examplePayload = { exampleName: exampleValue };

    it('action test ' + types.UPDATE_RESERVATION_FORM_STATE, () => {
        const expected = {
            type: types.UPDATE_RESERVATION_FORM_STATE,
            payload: examplePayload
        };
        expect(actions.updateReservationFormState(examplePayload)(e => e))
            .toEqual(expected);
    });

    it('action test ' + types.CLEAR_RESERVATION_FORM_STATE, () => {
        const expected = {
            type: types.CLEAR_RESERVATION_FORM_STATE,
        };
        expect(actions.clearReservationFormState()(e => e)).toEqual(expected);
    });
});