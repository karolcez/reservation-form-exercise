import * as types from './actionTypes';


export function updateReservationFormState(payload) {
        return function (dispatch) {
                return dispatch({ type: types.UPDATE_RESERVATION_FORM_STATE, payload });
        }
}

export function clearReservationFormState() {
        return function (dispatch) {
                return dispatch({ type: types.CLEAR_RESERVATION_FORM_STATE });
        }
}

