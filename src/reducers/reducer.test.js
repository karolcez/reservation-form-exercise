import expect from 'expect';
import reducer from './reducer';
import { initialState } from './reducer';
import * as types from '../actions/actionTypes';


describe('reducer basic tests', () => {

    const exampleName = Symbol();
    const exampleValue = Symbol();
    const examplePayload = {
        firstName: exampleValue,
    }
    const exampleState = {
        firstName: exampleValue,
        lastName: "",
        email: "",
        date: initialState.date,
    }

    it('returns the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    it('if no matching action returns the same state', () => {
        expect(reducer({ exampleName: exampleValue }, { type: undefined }))
            .toEqual({ exampleName: exampleValue });
    });

    it('reducer test ' + types.UPDATE_RESERVATION_FORM_STATE, () => {
        expect(reducer(initialState,
            {
                type: types.UPDATE_RESERVATION_FORM_STATE,
                payload: examplePayload
            }
        )).toEqual(exampleState);
    });

    it('reducer test ' + types.CLEAR_RESERVATION_FORM_STATE, () => {
        expect(reducer(exampleState,
            {
                type: types.CLEAR_RESERVATION_FORM_STATE,
            }
        )).toEqual(initialState);
    });
});
