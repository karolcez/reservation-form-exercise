import * as types from '../actions/actionTypes';
import moment from 'moment';

export const initialState = {
    firstName: '',
    lastName: '',
    email: '',
    date: moment(),
};


export default function (state = initialState, action) {
    switch (action.type) {
        case types.UPDATE_RESERVATION_FORM_STATE:
            return {
                ...state,
                ...action.payload
            };
        case types.CLEAR_RESERVATION_FORM_STATE:
            return {
                ...state,
                ...initialState
            };

        default:
            return state;

    }
}